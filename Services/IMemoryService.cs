﻿
namespace MemoryPoco.Services
{
    public interface IMemoryService
    {
        long SetObjectsMemory();
        long SetObjectsMemoryFactory();
    }
}
