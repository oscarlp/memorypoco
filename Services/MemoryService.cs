﻿using MemoryPoco.Helpers;
using System;
using System.Collections.Generic;

namespace MemoryPoco.Services
{
    public class MemoryService : IMemoryService
    {
        private readonly TreeFactory _treeFactory;
        public MemoryService()
        {
            _treeFactory = new TreeFactory();
        }

        //crea objetos en memoria sin patrones
        public long SetObjectsMemory()
        {
            List<Tree> trees = new List<Tree>();
            for (int i = 0; i < 5000; i++)
            {
                var tree = new Tree()
                {
                    Id = new Random().Next(1, 9999999),
                    Part = new PartTree()
                    {
                        Name = "Nameany",
                        Bark = "Barkany",
                        Color = "Colorany"
                    }
                };
                trees.Add(tree);
            };

            return Utilities.GetCurrentMemoryUsed();
        }

        //crea objetos en memoria usando patron flyweight
        public long SetObjectsMemoryFactory()
        {
            List<Tree> trees = new List<Tree>();
            for (int i = 0; i < 5000; i++)
            {
                var tree = new Tree()
                {
                    Id = new Random().Next(1, 9999999),
                    Part = _treeFactory.GetPartTree("Nameany", "Barkany", "Colorany")
                };
                trees.Add(tree);
            }

            return Utilities.GetCurrentMemoryUsed();
        }
    }
}
