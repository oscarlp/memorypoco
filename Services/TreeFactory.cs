﻿using System.Collections.Generic;
using System.Linq;

namespace MemoryPoco.Services
{
    public class TreeFactory
    {
        private static List<PartTree> _parts;

        public TreeFactory() {
            _parts = new List<PartTree>();
        }
        public PartTree GetPartTree(string name, string bark, string color)
        {
            //si existe un objeto de ese tipo lo retornamos, sino lo creamosy retornamos
            if (_parts.Any(x => x.Name == name && x.Bark == bark && x.Color == color))
            {
                return _parts.Where(x => x.Name == name && x.Bark == bark && x.Color == color).FirstOrDefault();
            }
            else {
                var newpart = new PartTree()
                {
                    Name = name,
                    Bark = bark,
                    Color = color
                };
                _parts.Add(newpart);
                return newpart;
            }
            
        }
    }
}
