﻿using System;
using System.Diagnostics;

namespace MemoryPoco.Helpers
{
    public static class Utilities
    {
        public static long GetCurrentMemoryUsed() {
            Int64 memory;
            using (Process proc = Process.GetCurrentProcess())
            {
                memory = proc.PrivateMemorySize64 / (1024 * 1024);
            }

            return memory;
        }
    }
}
