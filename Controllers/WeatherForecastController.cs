﻿using MemoryPoco.Helpers;
using MemoryPoco.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MemoryPoco.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;
        private readonly IMemoryService _memoryService;
        public WeatherForecastController(ILogger<WeatherForecastController> logger, IMemoryService memoryService)
        {
            _logger = logger;
            _memoryService = memoryService;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }

        [HttpGet, Route(nameof(WeatherForecastController.GenerateMemory))]
        public IActionResult GenerateMemory()
        {
            var total=_memoryService.SetObjectsMemory();
            return Ok(total);
        }

        [HttpGet, Route(nameof(WeatherForecastController.GenerateLiftMemory))]
        public IActionResult GenerateLiftMemory()
        {
            var total = _memoryService.SetObjectsMemoryFactory();
            return Ok(total);
        }

        [HttpGet, Route(nameof(WeatherForecastController.GetMemory))]
        public IActionResult GetMemory()
        {
            var total = Utilities.GetCurrentMemoryUsed();
            return Ok(total);
        }
    }
}
